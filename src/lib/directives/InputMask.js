import BgInputMask from '../utils/BgInputMask';

const directiveFn = () => {
  const eventList = [
    ['keydown', 'inputEvent'],
    ['paste', 'pasteEvent'],
    ['cut', 'cutEvent'],
    ['mouseover', 'mouseoverEvent'],
    ['mouseout', 'mouseoutEvent'],
    ['focus', 'focusEvent'],
    ['blur', 'blurEvent'],
  ];

  let mask = null;
  return {
    isLiteral: true,
    bind(element, binding, vmNode) {
      const options = typeof binding.value === 'object' ? binding.value : {
        mask: binding.value,
        showPlaceholder: false,
        showMaskOnValue: false,
      };
      mask = new BgInputMask({
        element,
        vmNode,
        mask: options.mask,
        value: element.value,
        showPlaceholder: options.showPlaceholder,
        showMaskOnValue: options.showMaskOnValue,
        onUnMask: options.onUnMask,
      });
      eventList.forEach(([eventName, eventFn]) => {
        mask.element.addEventListener(eventName, mask[eventFn]);
      });
    },
    unbind() {
      eventList.forEach(([eventName, eventFn]) => {
        mask.element.removeEventListener(eventName, mask[eventFn]);
      });
    },
    componentUpdated() {
      eventList.forEach(([eventName, eventFn]) => {
        mask.element.removeEventListener(eventName, mask[eventFn]);
        mask.element.addEventListener(eventName, mask[eventFn]);
      });
    },
  };
};

export default directiveFn();
