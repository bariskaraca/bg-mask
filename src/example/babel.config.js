module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset',
    [
      '@babel/env',
      {
        useBuiltIns: 'usage',
        corejs: 3,
      },
    ],
  ],
  plugins: [
    ['@babel/plugin-proposal-optional-chaining'],
  ],
};
