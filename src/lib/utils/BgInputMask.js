export default class BgInputMask {
  constructor({
    value,
    mask,
    placeholderChar,
    element,
    showPlaceholder,
    showMaskOnValue,
    onUnMask,
    vmNode,
  }) {
    this.regexMap = {
      ESCAPE_CHAR: '\\',
      NUMBER: /^\d$/,
      LETTER: /^[A-Za-z]$/,
      ALPHA_NUMERIC: /^[\dA-Za-z]$/,
    };
    this.mask = mask;
    this.value = [];
    this.initialValue = (value || '').split('');
    this.placeholderChar = placeholderChar || '_';
    this.showPlaceholder = showPlaceholder || false;
    this.showMaskOnValue = showMaskOnValue || false;
    this.onUnMask = onUnMask || false;
    this.definitons = {
      '*': {
        validate: (char) => this.regexMap.ALPHA_NUMERIC.test(char),
        transform: (char) => char,
      },
      1: {
        validate: (char) => this.regexMap.NUMBER.test(char),
        transform: (char) => char,
      },
      a: {
        validate: (char) => this.regexMap.LETTER.test(char),
        transform: (char) => char,
      },
      A: {
        validate: (char) => this.regexMap.LETTER.test(char),
        transform: (char) => char.toUpperCase(),
      },
      '#': {
        validate: (char) => this.regexMap.ALPHA_NUMERIC.test(char),
        transform: (char) => char.toUpperCase(),
      },
    };
    this.editableIndexes = [];
    this.element = BgInputMask._getInputElement(element);
    this.vmNode = vmNode;

    this.pattern = [];
    this.inputEvent = this.onInputListener.bind(this);
    this.pasteEvent = this.onPasteListener.bind(this);
    this.cutEvent = this.onCutListener.bind(this);
    this.mouseoverEvent = this.onMouseoverListener.bind(this);
    this.mouseoutEvent = this.onMouseoutListener.bind(this);
    this.focusEvent = this.onFocusListener.bind(this);
    this.blurEvent = this.onBlurListener.bind(this);

    this.elementPlaceholder = this.vmNode?.data?.attrs?.placeholder || '';

    this.hover = false;
    this.focus = false;
    this._init();
  }

  static _getInputElement(element) {
    if (element.tagName.toLocaleUpperCase() === 'INPUT') {
      return element;
    }

    const inputElements = element.getElementsByTagName('input');
    if (inputElements.length === 0) {
      throw new Error('[bg-mask]: v-mask directive requires input element');
    }

    return inputElements[0];
  }

  get hover() {
    return this._hover;
  }

  set hover(val) {
    this._hover = val;
    this._setPlaceholder();
  }

  get focus() {
    return this._focus;
  }

  set focus(val) {
    this._focus = val;
    this._setPlaceholder();
  }

  _init() {
    const maskChars = this.mask.split('');

    let isEscaped = false;
    this.pattern = maskChars.reduce((res, char, idx) => {
      if (char === this.regexMap.ESCAPE_CHAR) {
        isEscaped = true;
        if (idx === maskChars.length - 1) {
          throw new Error(`mask can not ends with ${this.regexMap.ESCAPE_CHAR}`);
        }
        return res;
      }
      res.push({
        char,
        isEditable: char in this.definitons && !isEscaped,
      });
      isEscaped = false;
      return res;
    }, []);

    this.placeholder = this.pattern.map((p) => (p.isEditable ? this.placeholderChar : p.char)).join('');

    if (this.initialValue.length) {
      this._pasteWord(this.initialValue, { start: 0, end: this.initialValue.length });
    }
  }

  _backspace(selection) {
    if (selection.start === selection.end) {
      if (selection.start > 0) {
        this.value[selection.start - 1] = '';
      }
    } else {
      this.value = this.value.map((v, i) => {
        if (i >= selection.start && i < selection.end) {
          return '';
        } return v;
      });
    }
    return {
      start: selection.start > 0 ? selection.start - 1 : selection.start,
      end: selection.start > 0 ? selection.start - 1 : selection.start,
    };
  }

  _fireInputEvent(inputEvent = true) {
    let unmaskedValue = this.value
      .map((v, i) => {
        if (this.pattern[i].isEditable) {
          return v;
        }
        return false;
      })
      .filter((v) => v !== false)
      .join('');

    if (typeof this.onUnMask === 'function') {
      unmaskedValue = this.onUnMask(this._getValue(), unmaskedValue);
    }

    if (inputEvent) {
      const event = document.createEvent('Event');
      event.initEvent('input', true, true);
      this.element.dispatchEvent(event);
    }
    if (this.vmNode.componentInstance) {
      this.vmNode.componentInstance.$emit('maskInput', {
        detail: { unmaskedValue, value: this._getValue(), completed: false },
      });
    } else {
      this.element.dispatchEvent(new CustomEvent('maskInput', {
        detail: { unmaskedValue, value: this._getValue(), completed: false },
      }));
    }
  }

  _formatInput(charOrWord, selection) {
    const addedChars = [];
    let charIndex = 0;
    const isWord = charOrWord.length > 1;

    this.value = this.value.map((v, i) => {
      if (i >= selection.start && i < selection.end) {
        return '';
      } return v;
    });

    for (let i = selection.start; i < this.pattern.length; i++) {
      const char = charOrWord[charIndex];
      const currentPattern = this.pattern[i];
      if (currentPattern.isEditable) {
        const _char = this.definitons[currentPattern.char].transform(char);
        if (this.definitons[currentPattern.char].validate(_char)) {
          this.value[i] = _char;
          addedChars.push(_char);
          charIndex++;
        } else if (isWord) {
          break;
        }
        if (!isWord) {
          break;
        }
      } else if (currentPattern.char === char) {
        this.value[i] = char;
        addedChars.push(char);
        charIndex++;
        if (!isWord) {
          break;
        }
      } else {
        this.value[i] = currentPattern.char;
        addedChars.push(currentPattern.char);
      }
      if (isWord && charIndex > charOrWord.length - 1) {
        break;
      }
    }

    return {
      start: selection.start + addedChars.length,
      end: selection.start + addedChars.length,
    };
  }

  _getSelection() {
    return {
      start: this.element.selectionStart,
      end: this.element.selectionEnd,
    };
  }

  _getValue() {
    return this.showMaskOnValue
      ? this.pattern
        .map((p, i) => this.value[i] || (p.isEditable ? this.placeholderChar : p.char))
        .join('')
      : this.value.join('');
  }

  _getFirstEditableIndex() {
    return this.pattern.findIndex((p, idx) => p.isEditable
      && (!this.value[idx] || (this.showMaskOnValue && this.value[idx] === this.placeholderChar)));
  }

  static _isCharacterKeyPress(event) {
    const { keycode, key } = event;
    // [\]' (in order)
    return (keycode > 47 && keycode < 58) // number keys
      || keycode === 32 || keycode === 13 // spacebar & return key(s)
      || (keycode > 64 && keycode < 91) // letter keys
      || (keycode > 95 && keycode < 112) // numpad keys
      || (keycode > 185 && keycode < 193) // ;=,-./` (in order)
      || (keycode > 218 && keycode < 223)
      || key === 'Backspace'; // backspace;
  }

  _pasteWord(word, selection) {
    const selectionAfterInput = this._formatInput(word, selection);

    this.element.value = this._getValue();
    this._fireInputEvent();

    this.element.selectionStart = selectionAfterInput.start;
    this.element.selectionEnd = selectionAfterInput.end;
  }

  onInputListener(event) {
    if (event.metaKey || event.ctrlKey
      || (event.key.length !== 1 && !BgInputMask._isCharacterKeyPress(event))) {
      return;
    }
    event.preventDefault();

    const char = event.key;
    const selection = this._getSelection();
    let selectionAfterInput;

    if (char === 'Backspace') {
      selectionAfterInput = this._backspace(selection);
    } else { selectionAfterInput = this._formatInput(char, selection); }

    this.element.value = this._getValue();
    this._fireInputEvent();
    this.element.selectionStart = selectionAfterInput.start;
    this.element.selectionEnd = selectionAfterInput.end;
  }

  onPasteListener(event) {
    event.preventDefault();
    const word = (event.originalEvent || event).clipboardData.getData('text/plain');
    const selection = this._getSelection();

    this._pasteWord(word, selection);
  }

  onCutListener() {
    const selection = this._getSelection();
    this.value = this.value.map((v, i) => {
      if (i >= selection.start && i < selection.end) {
        return '';
      } return v;
    });
    this._fireInputEvent(false);
  }

  onMouseoverListener() {
    this.hover = true;
  }

  onMouseoutListener() {
    this.hover = false;
  }

  onFocusListener() {
    this.focus = true;

    const caretIndex = this._getFirstEditableIndex();
    setTimeout(() => {
      if (caretIndex >= 0) {
        this.element.selectionStart = caretIndex;
        this.element.selectionEnd = caretIndex;
      }
    }, 0);
  }

  onBlurListener() {
    this.focus = false;
  }

  _setPlaceholder() {
    const hoverPlaceholder = this.hover && (this.showPlaceholder === 'hover' || this.showPlaceholder === true);
    const focusPlaceholder = this.focus && (this.showPlaceholder === 'focus' || this.showPlaceholder === true);
    if (hoverPlaceholder || focusPlaceholder) {
      this.element.placeholder = this.placeholder;
    } else if (!hoverPlaceholder && !focusPlaceholder) {
      this.element.placeholder = this.elementPlaceholder;
    }
  }
}
