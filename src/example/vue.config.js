console.log(process.env.APP_MODE);
module.exports = {
  css: {
    extract: false,
  },
  publicPath: process.env.APP_MODE === 'example'
    ? '/bg-mask/'
    : '/',
};
