import Vue from 'vue';
import InputMask from '../lib/index';
import App from './App.vue';

Vue.config.productionTip = false;

Vue.use(InputMask);

new Vue({
  render: (h) => h(App),
}).$mount('#app');
